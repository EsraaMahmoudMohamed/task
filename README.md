Uber Task
contain Three web serives API
the first one to get driver access token by login 
the second one  to Add new trip
the third one to get list of best drivers in  this month, this year,all times

Installation

1-Clone project:

	git clone https://EsraaMahmoudMohamed@bitbucket.org/EsraaMahmoudMohamed/task.git

2-Change directory to project root

	cd task/task1

3- Run composer install

	composer install

4- make a copy of the .env.example file and rename it to .env inside your project root
 inside .env add your database info :

		DB_DATABASE=Your Database Name
		DB_USERNAME=Your Database Username
		DB_PASSWORD=Your Database Password
                
           ex:
		 DB_DATABASE=Task
		 DB_USERNAME=root
		 DB_PASSWORD=123

5-Run the following command to generate your app key:
	
	php artisan key:generate

6-Run migration With seed:

	php artisan migrate --seed

7-Run passport install:

	php artisan passport:install

Then start your server:

	php artisan serve


For test API from postman see documentation named Uber_task_api.docx INSIDE task 

