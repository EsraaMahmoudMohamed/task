<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class TripHelper {
    /**
     * generate code for trip
     * @return type
     */
 public static function generateCode() {
        $dayOrderPrefix = date('ymd');
        $lastDayOrderNumber = DB::table('trips')
                ->select(DB::raw('RIGHT(code, 4) as code'))
                ->where(DB::raw('LEFT(code, 6)'), $dayOrderPrefix)
                ->orderBy('id', 'desc')
                ->limit(1)
                ->value('code');
        return $dayOrderPrefix . str_pad($lastDayOrderNumber + 1, 4, '0', STR_PAD_LEFT);
    }
}