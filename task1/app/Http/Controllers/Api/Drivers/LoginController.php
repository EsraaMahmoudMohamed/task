<?php

namespace App\Http\Controllers\Api\Drivers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {

    use AuthenticatesUsers;

    /**
     * driver get access token by login 
     * @param Request $request
     * username,password
     * @return  string JSON that indicates success/failure of the driver request,
*                or JSON that indicates an error occurred
     */
    public function login(Request $request) {

        $valid_user = Auth::attempt(['username' => $request->username, 'password' => $request->password, 'active' => 1]);
        if (!$valid_user || is_null(Auth::user())) {
            return response()->json(['message' => 'Unauthorized', 'success' => false], 401);
        }
        $driver = User::find(Auth::user());
        $token = Auth::user()->createToken('loginToken')->accessToken;
        return response()->json(['token' => $token,'message' => "Login successfully", 'success' => true]);
        
    }

}
