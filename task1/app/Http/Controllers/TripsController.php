<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Trips;

class TripsController extends Controller {

    /**
     * Add new trip using driver data
     * @param Request $request
     * @return json
     */
    public function store(Request $request) {
        $driver_id = $request->user()->id;
        $trip = new Trips();
        $new_trip = $trip->newTrip($request->user());
        return $new_trip;
    }

    /**
     * get list of best  drivers in this month,year,all times
     * @param Request $request
     * @return json 
     */
    public function get_statistical(Request $request) {
        $trip = new Trips();
        $driver_satatistical = $trip->getStatistical();
        return $driver_satatistical;
    }

}
