<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\TripHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Trips extends Model {

    /**
     * fillable columns
     * @var type 
     */
    protected $fillable = [
        'driver_id', 'driver_full_name', 'code'];

    /**
     * relation between users and trips

     * @return type
     */
    public function driver() {
        return $this->belongsTo(User::class);
    }

    /**
     * add new trip for specific driver
     * @param type $driverData
     * use 2 parameter from $driverData (id,full_name)
     * using helper function to generate trip code 
     * return string JSON that indicates success/failure of the get statistical request
     */
    public function newTrip($driverData) {
        if (isset($driverData)) {
            $this->driver_id = $driverData->id;
            $this->driver_full_name = $driverData->full_name;
            $this->code = TripHelper::generateCode();
            $save = $this->save();
            $resp = $save ? ["code" => 200, "message" => 'Trip Added Successfully','success'=>true] : ["code" => 400, "message" => __("Cannot Add Trip"),'success' => false];
            return $resp;
        }
    }

    /**
     * get list of best drivers under specific condition and specific period
     * @param type $total_trip_in_this_period 
     * @param type $condition
     * @return json contain array of object 
     * ex [{"Driver Name":"test","TotalTrips":10}]
     */
    public function getBestDrivers($total_trip_in_this_period = NULL, $condition = NULL) {
        $qb = DB::table('trips')
                ->select('driver_full_name as Driver Name', DB::Raw("count(*) as TotalTrips"));
        if (!is_null($condition)) {
            $qb->where(DB::Raw("$condition(created_at)"), DB::Raw("$condition(CURRENT_DATE())"));
        }
        $qb->groupby('Driver Name');
        if (!is_null($total_trip_in_this_period)) {
            $qb->having('TotalTrips', '>=', ($total_trip_in_this_period * 10) / 100);
        }
        $best_drivers = $qb->orderBy('TotalTrips', 'desc')->get();

        return $best_drivers->toJson();
    }

    /**
     * get driver statistical for this month ,this year,all times that exceeding 10% of trips in this period
     * @return  string JSON that indicates success/failure of the get statistical request
     * JSON content total_no_of_trip this_month,this_year,all_times and best drivers exceeding 10% of trips
     */
    public function getStatistical() {
        $total_trip_in_this_month = Trips::where(DB::Raw("MONTH(created_at)"), DB::Raw("MONTH(CURRENT_DATE())"))->count();
        $total_trip_in_this_year = Trips::where(DB::Raw("YEAR(created_at)"), DB::Raw("YEAR(CURRENT_DATE())"))->count();
        $total_trip_all_time = Trips::count();
        $best_drivers_this_month = $this->getBestDrivers($total_trip_in_this_month, 'Month');
        $best_drivers_this_year = $this->getBestDrivers($total_trip_in_this_year, 'YEAR');
        $best_drivers_all_times = $this->getBestDrivers($total_trip_all_time);
        return ["code" => 200,"success"=>true, "total_trip_in_this_month" => $total_trip_in_this_month,
            'total_trip_in_this_year' => $total_trip_in_this_year,
            'total_trip_all_time' => $total_trip_all_time,
            'best_drivers_this_month' => $best_drivers_this_month,
            'best_drivers_this_year' => $best_drivers_this_year,
            'best_drivers_all_the_times' => $best_drivers_all_times];
    }

}
