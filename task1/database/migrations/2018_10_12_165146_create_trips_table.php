<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->nullable()->unsigned();
            $table->string('driver_full_name');
            $table->string('code')->unique();
            $table->foreign('driver_id')
                    ->references('id')->on('users')
                    ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('trips');
    }

}
