<?php

use Illuminate\Database\Seeder;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentDateTimeObj = \Carbon\Carbon::now();
        $currentDateTime = $currentDateTimeObj->toDateTimeString();
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1708120001",
                'created_at' => "2017-08-12 20:47:57",
                'updated_at' => "2017-08-12 20:47:57",
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1708120002",
                'created_at' => "2017-08-12 20:47:57",
                'updated_at' => "2017-08-12 20:47:57",
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1710120003",
               'created_at' => "2017-10-12 20:47:57",
                'updated_at' => "2017-10-12 20:47:57",
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120005",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120006",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120007",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120008",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120009",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120010",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120011",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120012",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120013",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120014",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120015",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120016",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120017",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120018",
                 'created_at' => "2018-08-12 20:47:57",
                'updated_at' => "2018-08-12 20:47:57",
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120019",
                'created_at' => "2018-08-12 20:47:57",
                'updated_at' => "2018-08-12 20:47:57",
            ],
        ]);
         DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120020",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120021",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120022",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120023",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120024",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120025",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120026",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120027",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120028",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120029",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120030",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120031",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120032",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120033",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120034",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120035",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120036",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120037",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120038",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120039",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120040",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120041",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120042",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120043",
               'created_at' => "2017-08-12 20:47:57",
                'updated_at' => "2017-08-12 20:47:57",
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120044",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120045",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120046",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120047",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120048",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120049",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120050",
                'created_at' => "2018-08-12 20:47:57",
                'updated_at' => "2018-08-12 20:47:57",
            ],
        ]);
        DB::table('trips')->insert([
            [
               'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120060",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120061",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120062",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120063",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120064",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120065",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120066",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120067",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120068",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120069",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120070",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 2,
                'driver_full_name' => "Eslam Mahmoud",
                'code' =>"1810120071",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120072",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120073",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120074",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 3,
                'driver_full_name' => "Aya Mahmoud",
                'code' =>"1810120075",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('trips')->insert([
            [
                'driver_id' => 1,
                'driver_full_name' => "Esraa Mahmoud",
                'code' =>"1810120080",
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);

    }
}
