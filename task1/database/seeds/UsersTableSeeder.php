<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentDateTimeObj = \Carbon\Carbon::now();
        $currentDateTime = $currentDateTimeObj->toDateTimeString();
        DB::table('users')->insert([
            [
                'full_name' => "Esraa Mahmoud",
                'username' => "esraa",
                'password' => bcrypt(123456),
                'hiring_date'=>'2018-10-01',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('users')->insert([
            [
                'full_name' => "Eslam Mahmoud",
                'username' => "eslam",
                'password' => bcrypt(123123),
                'hiring_date'=>'2018-01-25',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('users')->insert([
            [
                'full_name' => "Aya Mahmoud",
                'username' => "aya",
                'password' => bcrypt(123123),
                'hiring_date'=>'2018-04-25',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('users')->insert([
            [
                'full_name' => "Mai Mahmoud",
                'username' => "mai",
                'password' => bcrypt(123123),
                'hiring_date'=>'2018-06-30',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
        DB::table('users')->insert([
            [
                'full_name' => "Yara Mahmoud",
                'username' => "yara",
                'password' => bcrypt(123123),
                'hiring_date'=>'2018-11-05',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
         DB::table('users')->insert([
            [
                'full_name' => "Sara Mahmoud",
                'username' => "sara",
                'password' => bcrypt(123123),
                'hiring_date'=>'2018-07-05',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
          DB::table('users')->insert([
            [
                'full_name' => "Sama Mahmoud",
                'username' => "sama",
                'password' => bcrypt(123123),
                'hiring_date'=>'2018-09-05',
                'created_at' => $currentDateTime,
                'updated_at' => $currentDateTime,
            ],
        ]);
    }
}
